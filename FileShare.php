<!DOCTYPE html>
<head><title>Log In</title></head>
<body>
        <h1>File Share</h1>
    
        <!--Existing user form-->
        <form action="Parse.php" method="get">
        <label>Enter Username: <input type="text" name="username"></label>
        <input type="submit" value="Log In">
        </form>
        
        <br>
        
        <!--Create new user form-->
        <form action="CreateUser.php" method="get">
        <label>New Username: <input type="text" name="newusername"></label>
        <input type="submit" value="Create User">
        </form>
        
        <br>
        
        <?php
        session_start();
        
        //If user is signed in, take them to home page
        if(!empty($_SESSION["username"])){
            header("Location: Main.php");
                exit;       
        }
        
        //Warning displayed if not a valid username
        //Or new username already exists
        echo $_SESSION['UserWarning'];
        
        ?>
        
    </body>
</html>