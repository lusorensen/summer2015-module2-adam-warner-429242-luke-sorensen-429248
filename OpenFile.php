<?php
  
session_start();
     
$filename = $_POST['file'];
     
  
//Check to see if filename is in a valid format.
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}
 
//Check if username follows an expected alphanumeric format.
$username = $_SESSION['username'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}
 
//Create file path.
$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
 
// Get MIME type.
$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($full_path);
 
// Set the Content-Type header to the correct MIME type and show file.
header("Content-Type: ".$mime);
readfile($full_path);
  
?>