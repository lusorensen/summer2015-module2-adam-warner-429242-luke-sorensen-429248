<?php

	session_start();
	
	$filename = $_POST['file'];  
	
	//Check to see if filename is in a valid format.
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		echo "Invalid filename";
		exit;
	}
	 
	//Check if username follows an expected alphanumeric format.
	$username = $_SESSION['username'];
	if( !preg_match('/^[\w_\-]+$/', $username) ){
		echo "Invalid username";
		exit;
	}
	
	//Get file path. 
	$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
	
	//Delete file, display error if not deleted.
	if (!unlink($full_path)){
		echo ("Error deleting $file");

	}else{
		echo ("Deleted $file");
	}
	echo "<br>";
	
	echo "<form action=\"Main.php\" method=\"get\">
    <input type=\"submit\" value=\"User page\">
    </form>";

?>