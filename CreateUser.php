<?php

session_start();

    $new= $_GET['newusername'];
    $filename = 'users.txt';
    
    $h = fopen("users.txt", "r");
            
    //Read through users.txt, redirect to log-in page with warning if entered username already exists.     
    while( !feof($h) ){
        $n= fgets($h);
            if(trim($n) == $_GET['newusername']) {
            
            $_SESSION['UserWarning']= "Username already exists";
            
            header("Location: FileShare.php");
            exit;
            }             
        }
        
    fclose($h);
        
    //Cheack if users.txt can be edited    
    if (is_writable($filename)) {
    
        if (!$handle = fopen($filename, 'a')) {
             echo "Cannot open file ($filename)";
             exit;
        }
    
        //Add user to users.txt doc
        if (fwrite($handle,"\n".$new) === FALSE) {
            echo "Cannot write to file ($filename)";
            exit;
        }
    
        echo "New user $new created";
    
        fclose($handle);
    
    } else {
        
        echo "Could not be added";
    }
    
    //Create directory in /srv/uploads/
    $full_path = sprintf("/srv/uploads/%s", $_GET['newusername']);
    mkdir($full_path, 0777, true);
    
    echo "<form action=\"FileShare.php\" method=\"get\">
        <input type=\"submit\" value=\"Login page\">";
    

?>