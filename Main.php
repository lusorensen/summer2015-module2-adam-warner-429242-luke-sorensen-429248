<!DOCTYPE html>
<head><title>File Share</title></head>

<body>
        
<?php

    session_start();

    $username = $_SESSION['username'];
    $handle = sprintf("/srv/uploads/%s/*",  $username);
    $num = 0;
    
    echo "<h1> $username </h1>";
    
    //Display list of files, along with buttons for viewing and deleting each file plus an option to share file.
    foreach(glob($handle) as $filename) {
        
        $wholefile = "$filename";
        
        //Open file form
        //Share file form with text input
        //Delete file form
        $new = str_replace(sprintf("/srv/uploads/%s/", $username), "", $wholefile);
        echo $new . "<form action=\"OpenFile.php\" method=\"POST\">
        <input type=\"hidden\" name=\"file\" value=$new />
        <input type=\"submit\" value=\"View File\" />
        </form>
        
        <form action=\"ShareFile.php\" method=\"GET\">
        <input type=\"hidden\" name=\"file\" value=$new />
        <input type=\"text\" name=\"shareuser\">
        <input type=\"submit\" value=\"Transfer File\" />
        </form>
        
        <form action=\"Deleter.php\" method=\"POST\">
        <input type=\"hidden\" name=\"file\" value=$new />
        <input type=\"submit\" value=\"Delete File\" />
        </form>";
        
        echo "<br>";
    }
        
?>
    
    
<!--Create option to upload a file to user's file directory.-->    
<form enctype="multipart/form-data" action="uploader.php" method="POST">
    <p>
        
    <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
    <label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
    
    </p>
    
    <p>
        <input type="submit" value="Upload File" />
    </p>
</form>

<!--Creates logout button.-->
<form action="Logout.php">
    <input type="submit" value="Log Out"/>
</form>

</body>
</html>

