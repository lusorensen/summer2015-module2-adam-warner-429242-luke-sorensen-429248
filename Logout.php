<?php

    session_start();
    
    session_destroy();
    
    //Take user back to log in page.
    header("Location: FileShare.php");
    exit;  

?>